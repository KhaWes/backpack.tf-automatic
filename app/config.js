const fs = require('fs');
const CONFIG_FILENAME = 'config.json';
const defaultConfig = {
    "dateFormat": "HH:mm:ss",
    "acceptOverpay": true,
    "acceptGifts": false,
    "declineBanned": true,
    "acceptEscrow": false,
    "acceptedKeys": [
        "Mann Co. Supply Crate Key"
    ],
    "logs": {
        "console": {
            "level": "verbose"
        },
        "file": {
            "filename": "automatic.log",
            "disabled": false,
            "level": "info"
        },
        "trade": {
            "filename": "automatic.trade.log",
            "disabled": false
        }
    },
    "steam": {
        "sentries": {},
        "identitySecret": {},
        "oAuthTokens": {}
    },
    "tokens": {}
};

let config = {};

exports.get = function() {
    return config;
};

exports.write = function(conf) {
    config = conf;
    fs.writeFileSync(CONFIG_FILENAME, JSON.stringify(config, null, "\t"));
};

exports.init = function () {
    let msg = "";
    
    if (fs.existsSync(CONFIG_FILENAME)) {
        try {
            config = JSON.parse(fs.readFileSync(CONFIG_FILENAME));
        } catch (e) {
            msg = "Cannot load " + CONFIG_FILENAME + ". " + e.toString() + ". Using default config.";
        }
    } else {
        exports.write(defaultConfig);
        msg = "Config generated.";
    }
    
    config.steam = config.steam || {"sentries": {}, "identitySecret": {}};
    config.tokens = config.tokens || {};
    return msg;
};