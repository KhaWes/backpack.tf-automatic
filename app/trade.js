const fs = require('fs');
const TradeOfferManager = require('steam-tradeoffer-manager');
const {getMetalValue} = require('./utils');
const backpack = require('./backpacktf');

const POLLDATA_FILENAME = 'polldata.json';

let manager, log, Config, steam, automatic;

exports.acceptOffer = acceptOffer;
exports.declineOffer = declineOffer;
exports.register = (Automatic) => {
    log = Automatic.log;
    manager = Automatic.manager;
    Config = Automatic.config;
    steam = Automatic.steam;
    automatic = Automatic;

    if (fs.existsSync(POLLDATA_FILENAME)) {
        try {
            manager.pollData = JSON.parse(fs.readFileSync(POLLDATA_FILENAME));
        } catch (e) {
            log.verbose("polldata.json is corrupt: ", e);
        }
    }

    manager.on('pollData', savePollData);
    manager.on('newOffer', handleOffer);
    manager.on('receivedOfferChanged', offerStateChanged);
};

function getOfferError(err) {
    let msg = err.cause || err.message;
    if (err.eresult) {
        msg = TradeOfferManager.EResult[err.eresult];
    }
    return msg;
}

function acceptOffer(offer) {
    return new Promise((resolve, reject) => {
        // status is one of "pending", "escrow", "accepted"
        offer.accept((err, status) => {
            if (err) {
                reject(getOfferError(err));
            } else {
                if (automatic.acceptTradeConfs) {
                    steam.checkConfirmations();
                }
                
                resolve(status);
            }
        });
    });
}

function declineOffer(offer) {
    return new Promise((resolve, reject) => {
        offer.decline((err) => {
            if (err) {
                reject(getOfferError(err));
            } else {
                resolve();
            }
        });
    });
}

function savePollData(pollData) {
    fs.writeFile(POLLDATA_FILENAME, JSON.stringify(pollData), (err) => {
        if (err) log.warn("Error writing poll data: " + err);
    });
}

function handleOffer(offer) {
    const steam3 = offer.partner.getSteam3RenderedID(),
        oid = "#" + offer.id;
    const config = Config.get();

    if (offer.isGlitched()) {
        log.info(`${steam3} Incoming offer from ${offer.partner.toString()} is glitched (Steam might be down).`);
        return;
    }

    log.info(`${steam3} Incoming offer ${oid} from ${offer.partner.toString()}`);

    if (offer.itemsToGive.length === 0 || offer.itemsToReceive.length === 0) {
        if (config.acceptGifts && offer.itemsToGive.length === 0 && offer.itemsToReceive.length > 0) {
            log.info(`${steam3} Offer ${oid} is a gift offer asking for nothing in return, will accept`);
            acceptOffer(offer).then((status) => {
                log.trade(`${steam3} Gift offer ${oid} successfully accepted${status === 'pending' ? "; confirmation required" : ""}`);
                log.debug("Gift offer: not sending confirmation to backpack.tf");
            }).catch((msg) => {
                log.warn(`${steam3} Unable to accept gift offer ${oid}: ${msg}`);
            });
        } else {
            log.info(`${steam3} Offer ${oid} is a gift offer, skipping`);
        }
        return;
    }

    let currencies = {
        "metal": 0,
        "keys": 0
    };


    // TODO: Buy orders
    for (let i = 0; i < offer.itemsToReceive.length; i += 1) {
        let item = offer.itemsToReceive[i];

        if (item.appid !== 440) {
            log.info(`${steam3} Offer ${offer} contains non-TF2 items, skipping`);
            return;
        }

        const metalValue = getMetalValue(item);
        if (metalValue > 0) {
            currencies.metal += metalValue;
        } else if (config.acceptedKeys && config.acceptedKeys.indexOf(item.market_hash_name) != -1) {
            currencies.keys += 1;
        }
    }

    // Fix x.99999 metal values
    if (currencies.metal % 1 >= 0.99) {
        currencies.metal = Math.round(currencies.metal);
    }

    backpack.handleOffer(offer, currencies);
}

function offerStateChanged(offer, oldState) {
    const oid = "#" + offer.id;
    log.verbose(`Offer ${oid} state changed: ${TradeOfferManager.ETradeOfferState[oldState]} -> ${TradeOfferManager.ETradeOfferState[offer.state]}`);

    if (offer.state === TradeOfferManager.ETradeOfferState.InvalidItems) {
        log.info(`Offer ${oid} is now invalid; declining`);
        declineOffer(offer).then(() => log.debug(`Offer ${oid} declined`));
    }
}

