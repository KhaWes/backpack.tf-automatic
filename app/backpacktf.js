const TradeOfferManager = require('steam-tradeoffer-manager');
const Utils = require('./utils');
const Trade = require('./trade');
let manager, steam, log, Config, automatic;

const {itemSummary} = Utils;

exports.heartbeat = heartbeat;
exports.register = register;
exports.handleOffer = handleOffer;
exports.acceptOffer = acceptOffer;
exports.getToken = getToken;

let offerSummaries = {}; // oid: summary

function getToken() {
    return new Promise((resolve, reject) => {
        Utils.prompt({
            "token": {
                "description": "backpack.tf token".green,
                "type": "string",
                "required": true,
                message: "Find yours at backpack.tf/settings > Advanced"
            }
        }).then((result) => {
            let config = Config.get();
            config.tokens = config.tokens || {};
            config.tokens[automatic.getOwnSteamID()] = result.token;
            Config.write(config);

            resolve(result.token);
        }).catch((err) => {
            Utils.fatal(log, "Cannot read backpack.tf token: " + err.message);
        });
    });
}

function onOfferChanged(offer, oldState) {
    if (offer.state === TradeOfferManager.ETradeOfferState.Accepted && offerSummaries[offer.id]) {
        notifyOfferAccepted(offer);
    }
}

function register(Automatic) {
    manager = Automatic.manager;
    steam = Automatic.steam;
    log = Automatic.log;
    Config = Automatic.config;
    automatic = Automatic;

    manager.on('receivedOfferChanged', onOfferChanged);
}

function heartbeat() {
    const token = automatic.getSavedToken();
    return new Promise((resolve, reject) => {
        Utils.postJSON({
            url: automatic.apiPath("IAutomatic/IHeartBeat"),
            form: {
                "method": "alive",
                "version": automatic.version,
                "steamid": steam.steamID.toString(),
                "token": token
            }
        }).then(([body]) => {
            if (!body.success) {
                log.warn(body.message || "Invalid backpack.tf token");

                if (body.message.match(/does not exist/)) {
                    log.warn("Maybe you entered your backpack.tf api key? Your third party access token can be found at backpack.tf/settings > Advanced.");
                }
                return reject("getToken");
            }

            let bumped = body.bumped;
            log.verbose(`Heartbeat sent to backpack.tf.${bumped ? ` ${bumped} listing${bumped === 1 ? '' : 's'} bumped.` : ""}`);
            resolve(1000 * 60 * 5);
        }).catch((msg, statusCode, body) => {
            log.warn("Error occurred contacting backpack.tf (" + msg + "), trying again in 1 minute");
            reject(1000 * 60 * 1);
        });
    });
}

function verifyItemsToGive(offer, currencies, listings) {
    const steam3 = offer.partner.getSteam3RenderedID();

    // Check if the offer would make us lose any items that aren't in a listing
    for (let i = 0; i < offer.itemsToGive.length; i++) {
        let item = offer.itemsToGive[i];

        if (item.appid !== 440) {
            log.info(`${steam3} Offer #${offer.id} contains non-TF2 items, skipping`);
            return false;
        }

        let itemIsChange = Utils.isMetal(item);
        if (itemIsChange) {
            currencies.metal -= Utils.getMetalValue(item);
        } else {
            const isListing = listings.some(function (listing) {  // Check if the item is in any listing
                return listing.item && listing.item.id == (item.assetid || item.id);
            });

            if (!isListing) {
                log.info(`${steam3} Offer #${offer.id} contains an item that isn't in a listing ${item.name}, skipping`);
                return false;
            }
        }
    }

    return true;
}

function determineEscrowDays(offer, my, them) {
    const myDays = my.escrowDays;
    const theirDays = them.escrowDays;
    let escrowDays = 0;

    if (offer.itemsToReceive.length > 0 && theirDays > escrowDays) {
        escrowDays = theirDays;
    }

    if (offer.itemsToGive.length > 0 && myDays > escrowDays) {
        escrowDays = myDays;
    }
    
    return escrowDays;
}

function checkEscrow(offer) {
	const acceptEscrow = Config.get().acceptEscrow,
        oid = offer.id;

	if (acceptEscrow === true) {
		return Promise.resolve(); // user doesn't care about escrow
	}

	return new Promise((resolve, reject) => {
		offer.getUserDetails((err, my, them) => {
			if (err) {
				return reject(err);
			}

            const escrowDays = determineEscrowDays(offer, my, them);
			if (escrowDays > 0) {
				if (acceptEscrow === "decline") {
					log.info(`Offer #${oid} would incur an escrow period, declining.`);
                    Trade.declineOffer(offer).then(() => log.debug(`Offer #${oid} declined`));
				} else {
					log.warn(`Offer #${oid} would incur up to ${escrowDays} escrow. Not accepting.`);
				}

				return reject("escrow");
			}

            resolve();
		});
	});
}

function acceptOffer(offer, currencies, requiredCurrencies) {
    const steam3 = offer.partner.getSteam3RenderedID();
    const oid = "#" + offer.id;
    let message = "";

    // Everything looks good
    log.debug(`${steam3} Offer ${oid}: Required = ${JSON.stringify(requiredCurrencies)}, offered = ${JSON.stringify(currencies)}`);
    log.trade(`${steam3} Everything in offer ${oid} looks good, accepting`);

    message = "Asked: ";

    for (let cur in requiredCurrencies) {
        let amount = requiredCurrencies[cur];

        if (amount !== 0) {
            message += `${amount} ${cur} `;
        }
    }

    message += "(" + itemSummary(offer.itemsToGive) + "). Offered: ";

    for (let cur in currencies) {
        let amount = currencies[cur];
        if (amount !== 0) {
            message += `${amount} ${cur} `;
        }
    }

    message += "(" + itemSummary(offer.itemsToReceive) + ").";

    log.trade(`${steam3} Offer ${oid} - ${message}`);

    offerSummaries[offer.id] = message;
    Trade.acceptOffer(offer).then((status) => {
        log.trade(`${steam3} Offer ${oid} successfully accepted${status === 'pending' ? "; confirmation required" : ""}`);
    }).catch((msg) => {
        log.warn(`${steam3} Unable to accept offer ${oid}: ${msg}`);
    });
}

// todo: refactor
function handleOffer(offer, currencies) {
    let requiredCurrencies = {
        "metal": 0,
        "keys": 0
    };

    return Utils.getJSON({
        url: automatic.apiPath("IGetUserTrades/v1"),
        qs: {
            "steamid": steam.steamID.toString(),
            "steamid_other": offer.partner.toString(),
            "ids": offer.itemsToGive.map((item) => {
                return item.assetid || item.id;
            })
        },
        checkResponse: true
    }).then(([body, response]) => {
        const steam3 = offer.partner.getSteam3RenderedID();

        if (response.other && (response.other.scammer || response.other.banned)) {
            const decline = Config.get().declineBanned;
            log.info(`${steam3} Sender of offer #${offer.id} is marked as a scammer or banned${decline ? ", declining" : ""}`);

            if (decline) {
                Trade.declineOffer(offer).then(() => log.debug(`Offer #${offer.id} declined`));
            }
            return;
        }

        let listings = response.store;
        if (listings.length === 0) {
            return log.info(`${steam3} No matching listings found for offer #${offer.id}, skipping`);
        }

        if (!verifyItemsToGive(offer, currencies, listings)) { // mutates currencies
            return;
        }

        for (let i in currencies) {
            if (currencies[i] < 0) {
                return log.info(`${steam3} Offer #${offer.id} tries to take too much ${i} from us, skipping`);
            }
        }

        // Add up the required price
        listings.forEach(function (listing) {
            for (let i in listing.currencies) {
                requiredCurrencies[i] += listing.currencies[i];
            }
        });

        const config = Config.get();
        for (let i in requiredCurrencies) {
            // Truncate everything past the second decimal place
            requiredCurrencies[i] = Math.floor(requiredCurrencies[i] * 100) / 100;

            if (requiredCurrencies[i] === 0) { // We don't need any of this currency
                continue;
            }

            if (currencies[i]) {
                currencies[i] = Math.floor(currencies[i] * 100) / 100;
            }

            let required = requiredCurrencies[i];
            let offered = currencies[i];
            if (!offered || offered < required) {
                return log.info(`${steam3} Offer #${offer.id} doesn't offer enough ${i} (offered = ${offered}, required = ${required}), skipping`);
            }

            if (!config.acceptOverpay && currencies[i] > requiredCurrencies[i]) {
                return log.info(`${steam3} Offer #${offer.id} offered too much ${i} (offered = ${offered}, required = ${required}), skipping`);
            }
        }

        finalizeOffer();
    }).catch((msg, statusCode, body) => {
        let m = msg;

        if (statusCode >= 500) {
            m = "backpack.tf is down (" + statusCode + ")";
        }

        log.warn("Error occurred getting trade list (" + m + "), trying again in 60 seconds");
        Utils.after.minutes(1).then(() => handleOffer(offer, currencies));
    });

    function finalizeOffer() {
        checkEscrow(offer).then(() => acceptOffer(offer, currencies, requiredCurrencies)).catch((err) => {
            if (err === "escrow") return;

            if (err.message === "Not Logged In") {
                log.warn("Cannot check escrow duration because we're not logged into Steam, retrying in 10s...");
            } else {
                log.error(`Cannot check escrow duration for offer #${offer.id}: ${err.message}`);
            }

            Utils.after.seconds(10).then(finalizeOffer);
        })
    }
}

function extractAssetInfo(item) {
    return {
        "appid": item.appid,
        "contextid": item.contextid,
        "assetid": item.assetid || item.id,
        "classid": item.classid,
        "instanceid": item.instanceid || "0",
        "amount": item.amount || "1",
        "missing": item.missing ? "true" : "false"
    };
}

function notifyOfferAccepted(offer) {
    let summary = offerSummaries[offer.id];
    if (!summary) {
        log.debug("Double confirming of offer " + offer.id + " canceled");
        return;
    }

    Utils.postForm({
        "url": automatic.apiPath("IAutomatic/IOfferDetails/"),
        "form": {
            "method": "completed",
            "steamid": automatic.getOwnSteamID(),
            "version": automatic.version,
            "token": automatic.getSavedToken(),
            "message": summary,
            "offer": {
                "tradeofferid": offer.id,
                "accountid_other": offer.partner.accountid,
                "steamid_other": offer.partner.getSteamID64(),
                "message": offer.message,
                "expiration_time": Math.floor(offer.expires.getTime() / 1000),
                "trade_offer_state": offer.state,
                "is_our_offer": offer.isOurOffer ? "true" : "false",
                "time_created": Math.floor(offer.created.getTime() / 1000),
                "time_updated": Math.floor(offer.updated.getTime() / 1000),
                "from_real_time_trade": offer.fromRealTimeTrade ? "true" : "false",
                "items_to_give": offer.itemsToGive.map(extractAssetInfo),
                "items_to_receive": offer.itemsToReceive.map(extractAssetInfo),
                "confirmation_method": offer.confirmationMethod || 0,
                "escrow_end_date": offer.escrowEnds ? Math.floor(offer.escrowEnds.getTime() / 1000) : 0
            }
        }
    }).then(() => {
        log.debug(`Notification sent to backpack.tf for offer #${offer.id}`);
        offerSummaries[offer.id] = null;
    })
    .catch((msg) => {
        log.warn("Cannot contact backpack.tf, retrying in 30 seconds... - " + msg);
        Utils.after.seconds(30).then(() => notifyOfferAccepted(offer));
    });
}
