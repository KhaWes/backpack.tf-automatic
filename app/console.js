var Readline = require('readline');
var input = Readline.createInterface({
    "input": process.stdin,
    "output": process.stdout
});

module.exports = input;
module.exports.start = (Automatic) => {
    const steam = Automatic.steam,
        Config = Automatic.config;

    input.on('line', function(line) {
        let config = Config.get(),
            parts = line.split(' ');

        switch (parts[0]) {
            case 'identity_secret':
                if (!parts[1]) {
                    console.log("Usage: identity_secret <base64 identity_secret for your account>");
                    break;
                }

                config.steam.identitySecret = config.steam.identitySecret || {};
                config.steam.identitySecret[Automatic.getOwnSteamID()] = parts[1];
                Config.write(config);

                steam.startConfirmationChecker(10000, parts[1]);
                Automatic.acceptTradeConfs = true;
                console.log("identity_secret saved. ALL trade confirmations will be automatically accepted.");
                break;
            case 'logout': {
                let username = config.steam.last;
                if (config.steam.oAuthTokens && config.steam.oAuthTokens[username]) {
                    delete config.steam.cookies;
                    delete config.steam.oAuthTokens[username];
                }

                Config.write(config);

                console.log("Logged out successfully.");
                process.exit(0);
                break;
            }
            case '.debug':
                // undocumented
                eval(parts.slice(1).join(' '));
                break;
            default:
                console.log("Unknown command '" + parts[0] + "'");
                // fallthrough
            case 'help':
                console.log("Commands: help, identity_secret, logout");
        }
    });
};