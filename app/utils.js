const request = require('request');
const Prompt = require('prompt');
require('colors');

// Set up prompt
Prompt.message = Prompt.delimiter = "";

exports.getJSON = function (opts, _json) {
    let o = {uri: opts.url, json: _json === false ? false : true};

    if (opts.qs) o.qs = opts.qs; // querystring

    return new Promise((resolve, reject) => {
        request.get(o, function (err, response, body) {
            const statusCode = exports.getStatusCode(response);
            if (err || statusCode !== 200 || (opts.checkResponse && !body.response && !body.response.success)) {
                const errorMsg = (err && err.message) || "HTTP error " + statusCode;
                return reject(errorMsg, statusCode, body);
            }

            resolve([body, body.response]);
        });
    });
};

exports.postJSON = function (opts, _json) {
    let o = {uri: opts.url, form: opts.form, json: _json === false ? false : true};

    return new Promise((resolve, reject) => {
        request.post(o, function (err, response, body) {
            const statusCode = exports.getStatusCode(response);
            if (err || statusCode !== 200) {
                const errorMsg = (err && err.message) || "HTTP error " + statusCode;
                return reject([errorMsg, statusCode, body]);
            }

            resolve([body, body.response]);
        });
    });
};

exports.postForm = (opts) => exports.postJSON(opts, false);

exports.getStatusCode = (response) => {
    return (response && response.statusCode) || "(unknown)";
};

exports.prompt = (props) => {
    return new Promise((resolve, reject) => {
        Prompt.start();
        Prompt.get({properties: props}, (err, result) => {
            if (err) reject(err);
            else resolve(result);
        });
    });
};

exports.fatal = (log, msg) => {
    log.error(msg);
    process.exit(1);
};

exports.itemIsUncraftable = (item) => {
    return (item.descriptions || []).some(function(description) {
         // ( Not Usable in Crafting )
        return description.value && description.value.match(/Usable in Crafting/i);
    });
}

exports.isWeapon = (item) => {
    var type = item.getTag('Type');
    if (!type) {
        return false;
    }

    if (item.market_hash_name.match(/(Class|Slot) Token/)) {
        return false;
    }

    if (exports.itemIsUncraftable(item)) {
        return false;
    }

    return ['primary weapon', 'secondary weapon', 'melee weapon', 'primary pda', 'secondary pda'].indexOf(type.name.toLowerCase()) != -1;
}

exports.getMetalValue = (item) => {
    if (exports.isWeapon(item)) {
        return 1/18;
    }

    switch ((item.market_hash_name || item.market_name || item)) {
        case "Scrap Metal":
            return 1/9;

        case "Reclaimed Metal":
            return 1/3;

        case "Refined Metal":
            return 1;
    }

    return 0;
};

exports.isMetal = (item) => {
    const name = item.market_hash_name || item.market_name || item;
    return name === "Scrap Metal" || name === "Reclaimed Metal" || name === "Refined Metal";
};

exports.itemSummary = (items) => {
    if (!items.length) {
        return "gift offer";
    }

    let names = {};
    
    items.forEach(function(item) {
        let name = item.market_hash_name;

        if (exports.itemIsUncraftable(item)) {
            name += " (uncraftable)";
        }

        names[name] = (names[name] || 0) + 1;
    });

    let formattedNames = [];
    for (const name in names) {
        formattedNames.push(name + (names[name] > 1 ? " x" + names[name] : ""));
    }

    return formattedNames.join(', ');
};

exports.after = {
    timeout(time) {
        return new Promise(resolve => setTimeout(resolve, time));
    },
    seconds(s) {
        return this.timeout(1000 * s);
    },
    minutes(m) {
        return this.timeout(1000 * 60 * m);
    }
};