const open = require('open');
const Utils = require('./utils');
const backpack = require('./backpacktf');

let client, log, Config, manager, automatic;

let g_Cookies = [];
let g_RelogInterval = null;

exports.register = (Automatic) => {
    client = Automatic.steam;
    log = Automatic.log;
    Config = Automatic.config;
    manager = Automatic.manager;
    automatic = Automatic;

    client.on('debug', function (msg) {
        log.debug(msg);
    });

    client.on('sessionExpired', relog);
};

exports.connect = () => {
    const config = Config.get();
    let username = config.steam ? config.steam.last : null;

    if (username && config.steam && config.steam.sentries && config.steam.sentries[username] && config.steam.oAuthTokens && config.steam.oAuthTokens[username]) {
        log.info("Logging into Steam with oAuth token");
        oAuthLogin(config.steam.sentries[username], config.steam.oAuthTokens[username]).then(handleLogin, handleLogin);
    } else {
        login();
    }
}

function oAuthLogin(sentry, token, quiet) {
    return new Promise((resolve, reject) => {
        client.oAuthLogin(sentry, token, (err, sessionID, cookies) => {
            if (err) {
                log.error("Cannot login to Steam: " + err.message);
                return reject("prompt");
            }

            if (!quiet) log.info("Logged into Steam!");
            client.setCookies(cookies);
            g_Cookies = cookies;
            resolve();
        });
    });
}

function handleLogin() {
    function unlock() {
        return new Promise((resolve) => {
            getFamilyViewPin().then(unlockFamilyView).then(resolve).catch(unlock);
        });
    }

    checkLogin()
        .then(setupManager)
        .catch((error) => {
            if (error === "prompt") {
                login().then(handleLogin);
            } else if (error === "retry") {
                Utils.after.seconds(5).then(handleLogin);
            } else if (error === "familyview") {
                unlock().then(handleLogin);
            }
        });
}

function checkLogin() {
    return new Promise((resolve, reject) => {
        client.loggedIn((err, loggedIn, familyView) => {
            if (err) {
                log.error("Cannot check Steam login: " + err);
                return reject("retry");
            }
            
            if (!loggedIn) {
                log.warn("Saved login cookies are no longer valid.");
                return reject("prompt");
            }

            if (familyView) {
                log.warn("This account is protected by Family View.");
                return reject("familyview");
            }
            
            resolve();
        });
    });
}

function login() {
    return getAccountDetails().then(rememberLast).then((details) => {
        const config = Config.get();
        let username = details.accountName;

        if (config.steam.sentries[username] && config.steam.oAuthTokens && config.steam.oAuthTokens[username]) {
            log.info("Logging into Steam with oAuth token");
            return oAuthLogin(config.steam.sentries[username], config.steam.oAuthTokens[username]).then(handleLogin).catch(() => performLogin(details).then(handleLogin));
        }

        return performLogin(details);
    });
}

function heartbeatLoop() {
    function loop(timeout) { setTimeout(heartbeatLoop, timeout); }
    backpack.heartbeat().then(loop, loop);
}

function setupManager() {
    const token = automatic.getSavedToken();
    if (!token) {
        return backpack.getToken().then(setupManager);
    }

    backpack.heartbeat().then((timeout) => {
        const config = Config.get();
        const sid = automatic.getOwnSteamID();

        if (config.steam.identitySecret && config.steam.identitySecret[sid]) {
            log.info("Starting Steam confirmation checker");
            client.startConfirmationChecker(10000, config.steam.identitySecret[sid]);
            automatic.acceptTradeConfs = true;
        } else {
            log.warn("Trade offers won't be confirmed automatically. In order to automatically accept offers, you should supply an identity_secret. See the readme.md file for more info on how to do this.");
        }

        log.debug("Setting Trade Offer Manager cookies.");
        manager.setCookies(g_Cookies, null, (err) => {
            if (err) {
                return Utils.fatal(log, "Cannot get API key: " + err.message);
            }

            log.info("Trade manager ready to go.");
            checkOfferCount();

            // Start the input console
            log.debug("Launching input console.");
            require('./console.js').start(automatic);
        });
        
        if (!g_RelogInterval) {
            g_RelogInterval = setInterval(relog, 1000 * 60 * 60 * 1); // every hour
        }
        setTimeout(heartbeatLoop, timeout);
    }).catch((timeout) => {
        if (timeout === "getToken") {
            backpack.getToken().then(setupManager);
        } else {
            Utils.after.timeout(timeout).then(setupManager);
        }
    });
}

function relog() {
    log.verbose("Renewing web session");

    const config = Config.get(),
        username = config.steam.last;

    oAuthLogin(config.steam.sentries[username], config.steam.oAuthTokens[username], true).then(() => {
        log.verbose("Web session renewed");
    }).catch((err) => {
        log.debug("Failed to relog (checking login): " + err.message);
        checkLogin()
            .then(() => log.verbose("Web session still valid"))
            .catch(() => log.warn("Web session no longer valid. Steam could be down or your session might no longer be valid. To refresh it, log out (type logout), restart Automatic, and re-enter your credentials"));
    });
}

function checkOfferCount() {
    return Utils.getJSON({
        url: "https://api.steampowered.com/IEconService/GetTradeOffersSummary/v1/?key=" + manager.apiKey
    }).then(([body, response]) => {
        if (!response) {
            log.warn("Cannot get trade offer count: malformed response");
            log.debug("apiKey used: %s", manager.apiKey);
            return;
        }

        let pending_sent = response.pending_sent_count,
            pending_received = response.pending_received_count;

        log.verbose(`${pending_received} incoming offer${pending_received === 1 ? '' : 's'} (${response.escrow_received_count} on hold), ${pending_sent} sent offer${pending_sent === 1 ? '' : 's'} (${response.escrow_sent_count} on hold)`);
    }).catch((msg, statusCode, body) => {
        log.warn("Cannot get trade offer count: " + msg);
        log.debug("apiKey used: %s", manager.apiKey);
    });
}

function getAccountDetails() {
    return new Promise((resolve, reject) => {
        const config = Config.get();
        Utils.prompt({
            "username": {
                "description": "Steam username".green,
                "type": "string",
                "required": true,
                "default": config.steam && config.steam.last
            },
            "password": {
                "description": "Steam password".green + " (hidden)".red,
                message: "Password is hidden",
                "type": "string",
                "required": true,
                "hidden": true
            }
        }).then((result) => {
            const accountName = result.username.toLowerCase(),
                password = result.password;
            let steamguard;

            if (config.steam && config.steam.sentries) {
                steamguard = config.steam.sentries[accountName];
            }

            resolve({accountName, password, steamguard});
        }).catch((err) => {
            Utils.fatal(log, "Cannot read Steam details: " + err.message);
        });
    });
}

function rememberLast(details) {
    const config = Config.get();
    config.steam = config.steam || {};
    config.steam.last = details.accountName;
    Config.write(config);
    return details;
}

function getSteamGuardCode(isMobile) {
    return new Promise((resolve, reject) => {
        // Steam Guard denied this login
        Utils.prompt({
            "authCode": {
                "description": ("Steam Guard" + (isMobile ? " app" : "") + " code").green,
                "type": "string",
                "required": true
            }
        }).then((result) => {
            resolve(result.authCode);
        }).catch((err) => {
            Utils.fatal(log, "Cannot read auth code: " + err.message);
        });
    });
}


function getCAPTCHA(url) {
    return new Promise((resolve, reject) => {
        open(url);
        Utils.prompt({
            "captcha": {
                "description": "CAPTCHA".green,
                "type": "string",
                "required": true
            }
        }).then((result) => {
            resolve(result.captcha);
        }).catch((err) => {
            Utils.fatal(log, "Cannot read CAPTCHA: " + err.message);
        });
    });
}

function getFamilyViewPin() {
    return new Promise((resolve, reject) => {
        return Utils.prompt({
            "pin": {
                "description": "Family View PIN (hidden)",
                "type": "string",
                "required": true,
                "hidden": true
            },
        }).then((result) => {
            resolve(result.pin);
        }).catch((err) => {
            Utils.fatal(log, "Cannot read PIN: " + err.message);
        });
    });
}

function performLogin(details) {
    client.login(details, (err, sessionID, cookies, steamguard, oAuthToken) => {
        if (err) {
            // There was a problem logging in
            let errcode = err.message;
            switch (errcode) {
                case 'SteamGuard':
                case 'SteamGuardMobile': {
                    let isMobile = errcode === "SteamGuardMobile";
                    getSteamGuardCode(isMobile).then((code) => {
                        details[isMobile ? "twoFactorCode" : "authCode"] = code;
                        performLogin(details);
                    });
                    break;
                }
                case 'CAPTCHA':
                    // We couldn't login because we need to fill in a captcha
                    getCAPTCHA(err.captchaurl).then((code) => {
                        details.captcha = code;
                        performLogin(details);
                    })
                    break;
                default:
                    // Some other error occurred
                    log.error("Login failed: " + errcode);
                    login();
            }
            return;
        }

        // Logged in successfully, ask the user if they want to remember this login
        require('prompt').confirm("Remember login?".green, {"default": "no"}, (err, save) => {
            const config = Config.get();
            let s = err ? false : save;

            if (s) {
                delete config.steam.cookies;
                config.steam.oAuthTokens = config.steam.oAuthTokens || {};
                config.steam.oAuthTokens[details.accountName] = oAuthToken;
                Config.write(config);
            }

            config.steam.sentries[details.accountName] = steamguard;
            g_Cookies = cookies;
            log.info("Logged into Steam!");

            handleLogin();
        });
    });
}

function unlockFamilyView(pin) {
    return new Promise((resolve, reject) => {
        client.parentalUnlock(pin, (err) => {
            if (err) {
                log.error("Unlock failed: " + err.message);
                reject(err);
            } else {
                resolve();
            }
        });
    });
}