const SteamCommunity = require('steamcommunity');
const TradeOfferManager = require('steam-tradeoffer-manager');
const Winston = require('winston');

const Utils = require('./utils');
const vers = (require('../package.json') || {version: "(unknown)", beta: ""});
let version = vers.version;
if (vers.beta && vers.beta !== version) {
    version = vers.beta + ' beta';
}

const Config = require('./config');
const logging = require('./logging');
const trade = require('./trade');
const steam = require('./steamclient');
const backpack = require('./backpacktf');

let configlog = Config.init();

let Automatic = {
    version,
    getSavedToken() {
        const config = Automatic.config.get();
        const sid = Automatic.getOwnSteamID();

        if (!config.tokens || !config.tokens[sid]) {
            return null;
        }

        return config.tokens[sid];
    },
    getOwnSteamID() {
        return Automatic.steam.steamID.getSteamID64();
    },
    apiPath(fn) {
        return (Automatic.config.get().backpackDomain || 'https://backpack.tf') + '/api/' + fn;
    },
    acceptTradeConfs: false
};

Automatic.config = Config;
Automatic.steam = new SteamCommunity();
Automatic.steam.username = null;
Automatic.manager = new TradeOfferManager({
    "language": "en",
    community: Automatic.steam,
    "domain": "backpack.tf",
    "pollInterval": 10500
});
let log = Automatic.log = new Winston.Logger({
    "levels": logging.LOG_LEVELS,
    "colors": logging.LOG_COLORS
});

logging.register(Automatic);
trade.register(Automatic);
backpack.register(Automatic);
steam.register(Automatic);

if (configlog) log.info(configlog);
log.info("backpack.tf Automatic v%s starting", version);

process.nextTick(steam.connect);

// Check if we're up to date
Utils.getJSON({
    url: "https://bitbucket.org/srabouin/backpack.tf-automatic/raw/master/package.json"
}).then(([body]) => {
    if (!body.version) {
        return log.warn("Cannot check for updates: malformed response");
    }

    let current = version.split('.');
    let latest = body.version.split('.');

    for (let i = 0; i < 3; i++) {
        if (current[i] < latest[i]) {
            log.info("===================================================");
            log.info("Update available! Current: v%s, Latest: v%s", version, body.version);
            log.info("===================================================");
            break;
        }
    }
}).catch((msg) => {
    log.warn("Cannot check for updates: " + msg);
});

process.on('uncaughtException', (err) => {
    log.error([
        "backpack.tf Automatic crashed! Please create an issue with the following log:",
        `crash: Automatic.version: ${Automatic.version}; node: ${process.version} ${process.platform} ${process.arch}; Contact: ${Automatic.getOwnSteamID()}`,
        `crash: Stack trace::`,
        require('util').inspect(err)
    ].join('\n'));
    log.error("Create an issue here: https://bitbucket.org/srabouin/backpack.tf-automatic/issues/new");
    process.exit(1);
})