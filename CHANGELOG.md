# 1.2.7
Fixed most session expiration issues.
Trade offer completion notifications are now only sent to backpack.tf when the offer went through (confirmed), not just after it's been accepted.

# 1.2.6
Fixed several bugs introduced in 1.2.5 as well as pre-existing ones
Added acceptEscrow: "decline" to automatically decline escrow offers.
Added custom log file names. Add "filename": "file" to logs.file and/or logs.trade. Any directories used must be created first.
Added a message to copy for crash reporting. These are saved to your Automatic log so you can report the crash after you closed the window.
The functionality of acceptGifts has been changed and is now more straightforward. Now, if it's true, gift offers where you receive items without giving any will be accepted.

Report issues here: https://bitbucket.org/srabouin/backpack.tf-automatic/issues/new